const {BrowserWindow, app} = require("electron");
const pie = require("puppeteer-in-electron")
const puppeteer = require("puppeteer-core");

const delay = (milliseconds) => new Promise((resolve) => setTimeout(resolve, milliseconds));

const main = async () => {
  await pie.initialize(app);
  const browser = await pie.connect(app, puppeteer);
 
  const window = new BrowserWindow();
  const url = "https://www.msc.com/en/search-a-schedule";
  await window.loadURL(url);
 
  const page = await pie.getPage(browser, window);
  console.log(page.url());

   await delay(3000);

    //await page.click("#onetrust-reject-all-handler")

    await page.type('#from','MUNDRA')

    await delay(2000);

    page.keyboard.press('Enter')

    await page.type('#to','DUBLIN')

    await delay(2000);

    page.keyboard.press('Enter')

    await delay(2000);

    await page.click(".grid-x > .cell > .msc-search-schedule__form:nth-child(2) > .msc-search-schedule__submit > .msc-cta")



 // window.destroy();
};

main();
